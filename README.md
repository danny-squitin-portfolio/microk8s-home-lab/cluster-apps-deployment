# Cluster Apps Deployment

The project manages the ArgoCD [app-of-apps](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/) pattern. The `argocd-bootstrap` application which is bootstrapped in the `argocd-bootstrap` directory is configured to load *this* application which loads all other applications.

Adding an Application CRD to this repository is the way to register an application with ArgoCD.

## Overview

The App of Apps pattern is a way to manage multiple applications as a single unit. It allows you to define and manage a set of applications using a declarative approach, making it easier to manage and deploy complex application stacks.

## Structure

The repository structure is as follows:

```sh
├── argocd-bootstrap
│   └── prod
│       ├── argocd-bootstrap.app.yaml
│       ├── argocd-bootstrap.appproj.yaml
│       ├── cluster-services-bootstrap.app.yaml
│       └── kustomization.yaml
└── cluster-services
    └── environments
        └── prod
            ├── argocd-deployment.app.yaml
            ├── cluster-services.appproj.yaml
            ├── kubernetes-dashboard.app.yaml
            └── kustomization.yaml
```
